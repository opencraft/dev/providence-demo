export const projectUrlFor = (path: string) => {
  const repo_url = process.env.REACT_APP_REPO_URL || 'https://gitlab.com/opencraft/dev/providence-demo/-/tree/main/'
  return `${repo_url}${path}`
}

export const docUrlFor = (path: string) => {
  const doc_root = process.env.REACT_APP_DOCS_URL || 'https://eye-of-providence.readthedocs.io/en/latest/'
  return `${doc_root}${path}`
}