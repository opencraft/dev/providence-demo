// https://reqres.in/ provides our test endpoint data here. Its product endpoint has the following structure by default.
export declare interface Product {
  id: number,
  year: number,
  name: string,
  color: string,
}