export default interface Register {
  username: string,
  email: string,
  age: number,
}