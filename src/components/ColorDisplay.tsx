declare interface ColorDisplayArguments {
  color: string,
}

export const ColorDisplay = ({color}: ColorDisplayArguments) => {
  return <div style={{display: 'inline-block', backgroundColor: color, width: '1em', height: '1em'}}/>
}