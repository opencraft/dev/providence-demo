# Providence demo application

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app). It exists to show off the basics of the [Eye of Providence state management library](https://eye-of-providence.readthedocs.io/en/latest/).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

## Live Demo

[You can view a live version of this demo here.](https://providence-demo.opencraft.com/)